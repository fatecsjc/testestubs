package br.edu.fatecsjc;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UsuarioTest {

    private static List<Usuario> usuarios;

    @BeforeAll
    public static void init() {

        usuarios = new ArrayList<>();

        Usuario usuario1 = new Usuario();
        usuario1.setId(1L);
        usuario1.setNome("Robson");

        Usuario usuario2 = new Usuario();
        usuario2.setId(2L);
        usuario2.setNome("Dilma");

        Usuario usuario3 = new Usuario();
        usuario3.setId(3L);
        usuario3.setNome("Guilherme");

        usuarios.add(usuario1);
        usuarios.add(usuario2);
        usuarios.add(usuario3);
    }

    @Test
    public void busca1() {

        Usuario usuarioEncontrado = null;

        for (Usuario usuario : usuarios) {

            if (usuario.getId().equals(1L)) {
                usuarioEncontrado = usuario;
                break;
            }
        }

        assertEquals(usuarioEncontrado, usuarios.get(0));
    }
}
