package br.edu.fatecsjc;

import java.util.List;

public interface UsuarioDAO {

    Usuario inserir(Usuario usuario);

    List<Usuario> buscar(Usuario usuario);
}
