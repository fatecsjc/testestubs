package br.edu.fatecsjc;

import java.util.ArrayList;
import java.util.List;

public class UsuarioDAOStub implements UsuarioDAO {

    public int contBuscar = 0;
    public int contInserir = 0;

    public UsuarioDAOStub() {
        this.contBuscar = 0;
        this.contInserir = 0;
    }

    public int getContBuscar() {
        return contBuscar;
    }

    public void setContBuscar(int contBuscar) {
        this.contBuscar = contBuscar;
    }

    public int getContInserir() {
        return contInserir;
    }

    public void setContInserir(int contInserir) {
        this.contInserir = contInserir;
    }

    @Override
    public Usuario inserir(Usuario usuario) {
        ++this.contInserir;

        switch (usuario.getNome()) {

            case "Robson":
                usuario.setId(1L);
                break;
            case "Dilma":
                usuario.setId(2L);
                break;
            case "Guilherme":
                usuario.setId(3L);
                break;
            default:
                return null;
        }

        return usuario;
    }

    @Override
    public List<Usuario> buscar(Usuario usuario) {
        ++this.contBuscar;

        List<Usuario> usuarios = new ArrayList<>();
        List<Usuario> usuariosEncontrados = new ArrayList<>();


        Usuario novoUsuario = new Usuario();
        novoUsuario.setId(1L);
        novoUsuario.setNome("Robson");

        usuarios.add(novoUsuario);

        for (Usuario ubd : usuarios){

            if (ubd.getNome().equals(novoUsuario.getNome()))
                usuariosEncontrados.add(ubd);
        }

        return usuariosEncontrados;
    }
}
