package br.edu.fatecsjc;

public class Usuario {

    private Long id;
    private String nome;

    public Usuario() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public boolean equals(Object obj) {

        if (obj instanceof Usuario) {
            Usuario usuario = (Usuario) obj;

            return this.id.equals(usuario.getId()) && this.nome.equals(usuario.getNome());
        }

        return false;
    }
}
