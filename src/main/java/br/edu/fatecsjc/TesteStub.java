package br.edu.fatecsjc;

import java.lang.reflect.Field;

public class TesteStub {

    public static void main(String[] args) {

        UsuarioDAO usuarioDAO = new UsuarioDAOStub();

        Usuario usuario = new Usuario();
        usuario.setNome("Robson");

        usuario = usuarioDAO.inserir(usuario);

        for (int i = 0; i < 100; ++i)
            usuario = usuarioDAO.inserir(usuario);

        System.out.println(usuario.getId());

        try {
            Field field1 = UsuarioDAOStub.class.getDeclaredField("contInserir");
            Field field2 = UsuarioDAOStub.class.getDeclaredField("contBuscar");
            System.out.println(field1.get(usuarioDAO));
            System.out.println(field2.get(usuarioDAO));
        }catch (Exception e){
            System.out.println("Erro de acesso ao campo.");
        }
    }
}
